﻿
#include <iostream>
using namespace std;

class Animal
{
public:
	virtual void Voice()
	{
		cout << "Animal\n";
	}
	Animal() { cout << "Animal()\n"; }
	virtual ~Animal() { cout << "~Animal()\n"; }
};

class Dog : public Animal
{
public:
	void Voice()
	{
		cout << "Woof!\n";
	}
	Dog() { cout << "Dog()\n"; }
	~Dog() { cout << "~Dog()\n"; }
};

class Cat : public Animal
{
public:
	void Voice()
	{
		cout << "Meow!\n";
	}
	Cat() { cout << "Cat()\n"; }
	~Cat() { cout << "~Cat()\n"; }
};

class Сhinchilla : public Animal
{
public:
	void Voice()
	{
		cout << "Kh-kh!\n";
	}
	Сhinchilla() { cout << "Chinchilla()\n"; }
	~Сhinchilla() { cout << "~Chinchilla()\n"; }
};
int main()
{
	Animal* p = new Dog;
	Animal* p1 = new Cat;
	Animal* p2 = new Сhinchilla;

	Animal* pArray[3] = { p, p1, p2 };

	for (int i = 0; i < 3; i++)
	{
		pArray[i]->Voice();
		delete pArray[i];
	}
	return EXIT_SUCCESS;
}
